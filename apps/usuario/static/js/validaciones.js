$("#formValiReg").validate({
    errorClass: 'invalid',
    rules: {
        name: {
            required: true,
            minlength: 10
        },
        username: {
            required: true,
            minlength: 5
        },
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
            minlength: 8,
        },
        password2: {
            required:true,
            equalTo: password,
        },
    },
    //For custom messages
    messages: {
        name: {
            required: "Ingrese su nombre",
            minlength: "Nombre incorrecto"
        },
        username: {
            required: "Ingrese un nombre de usuario",
            minlength: "Minimo 5 caracteres"
        },
        email: {
            required: "Ingrese correo electrónico",
            email: "Ingrese correo válido"
        },
        password: {
            required: "Ingrese contraseña",
            minlength: "Mínimo 8 caracteres"
        },
        password2:{
            required: "Repita contraseña",
            equalTo: "Clave diferente"
        }
    },
    errorElement: 'div',
    errorPlacement: function (error, element) {

        if (element.attr("type") == "radio") {
            error.insertBefore(element);
        } else {
            error.insertBefore(element);
        }

        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }


    }
});

function checkForm(form) {
    if (!form.acept.checked) {
        M.toast({html: 'Acepte los términos', classes: 'rounded' , classes: 'orange'});
        form.acept.focus();
        return false;
    }
    return true;
}




$("#formValiLog").validate({
    errorClass: 'invalid',
    rules: {
        username: {
            required: true
        },
        password: {
            required: true
        }
    },
    //For custom messages
    messages: {

        username: {
            required: "Ingrese un nombre de usuario",
        },
        password: {
            required: "Ingrese contraseña",
        }
    },
    errorElement: 'div',
    errorPlacement: function (error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});


$("#formforgetpass").validate({
    errorClass: 'invalid',
    rules: {
        email: {
            required: true
        },
    },
    //For custom messages
    messages: {
        email: {
            required: "Ingrese email valido",
        },
    },
    errorElement: 'div',
    errorPlacement: function (error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
});





$("#formValiReg").submit(function () {
    if ($("#formValiReg").valid()) {
        return true
    } else {
        Swal.fire({
            type: 'warning',
            title: 'Completa',
            text: 'No es posible registrar',
            footer: 'Completa todos los campos'
        })
    }
    return false
})

$("#formValiLog").submit(function () {
    if ($("#formValiLog").valid()) {
        return true
    } else {
        Swal.fire({
            type: 'error',
            title: 'ERROR',
            text: 'No es posible entrar',
            footer: 'Campos incorrectos o vacios'
        })
    }
    return false
})