document.addEventListener('keydown', function(evento){
    if(trex.suelo==170){
    if(evento.keyCode ==32){
        console.log("Come saludable!!");
        Saltar();
    //cuando aprete la tecla espacio, se escribira "come saludable!!""  
    }
}
});

var  nino,manzanaimg,nubeimg,sueloimg,sueloimg;

function cargaImagenes(){
 nino = new Image();
 nino.src = '../static/img/nino.png';

 sueloimg= new Image();
 sueloimg.src ='../static/img/suelo.png';

 manzanaimg = new Image();
 manzanaimg.src ='../static/img/Manzana.png';

 nubeimg = new Image();
 nubeimg.src = '../static/img/nube.png';

 fondoimg = new Image();
 fondoimg = '../static/img/suelo.png';
}

//resetear el canvas
var ancho = 700;
var alto = 300;
var canvas;
var op;

function inicializa(){
 canvas = document.getElementById('canvas');
 op = canvas.getContext('2d');

}

function borraCanvas(){
 canvas.width = ancho;
 canvas.height = alto;
 
}
      
//dibujar imagenes en las pociciones----------------------------------------
var suelo =170;
var trex = {suelo, vy:0, gravedad:2, salto:25, vymax:9,saltando:false};
var nivel = {velocidad: 9, marcador:0, muerto: false};
var manzana = {x: ancho + 100, y: suelo+60};
var nubee ={y: suelo-180};


function dibujarsuelo(){
    cargaImagenes();
    op.drawImage(sueloimg,0,0,100,100,0,suelo+100,800,50);
}

//function dibujarnube0(){
 //   cargaImagenes();
  //  op.drawImage(nubeimg,0,0,256,256,0,nubee.y,130,130);
//}
function dibujarfondo(){
    cargaImagenes();
    op.drawImage(fondoimg,0,50,626,427,100,0,0,0);
}

function dibujarnube1(){
    cargaImagenes();
    op.drawImage(nubeimg,0,50,256,256,100,nubee.y,130,130);
}
function dibujarnube2(){
    cargaImagenes();
    op.drawImage(nubeimg,0,50,256,256,200,nubee.y,130,130);
}
function dibujarnube3(){
    cargaImagenes();
    op.drawImage(nubeimg,0,50,256,256,300,nubee.y,130,130);
}
function dibujarnube4(){
    cargaImagenes();
    op.drawImage(nubeimg,0,50,256,256,400,nubee.y,130,130);
}
function dibujarnube5(){
    cargaImagenes();
    op.drawImage(nubeimg,0,50,256,256,500,nubee.y,130,130);
}
function dibujarnube6(){
    cargaImagenes();
    op.drawImage(nubeimg,0,50,256,256,600,nubee.y,130,130);
}

function dibujaninio(){
    cargaImagenes();
    op.drawImage(nino,20,22,360,360,15,trex.suelo,130,130);
   
}
   
function dibujamanzana(){
    cargaImagenes();
    op.drawImage(manzanaimg,0,0,90,120,manzana.x,manzana.y,50,40);
   }

function logicaManzana(){
 if(manzana.x < -100){
    manzana.x = ancho + 100;
    nivel.marcador++;
 } 
 else{
    manzana.x -= nivel.velocidad;
    }
}


//------------------------------------------------------------------------------
function Saltar(){
    trex.saltando = true;
    trex.vy = trex.salto;   
    }

function Gravedad(){
    if(trex.saltando == true){

        if(trex.suelo  -trex.vy - trex.gravedad> 170){
            trex.saltando = false;
            trex.vy =0;
            trex.y = 170;
        }
        else{
            trex.vy -= trex.gravedad;
            trex.suelo -= trex.vy;
        } 
    }
}

function colision(){
//chatarra

}

function puntuacacion(){
 op.font = "30px impact";
 op.fillStyle = '#555555';
 op.fillText(`${nivel.marcador}`,0,50);
}


//----------------------------------------
//bucle principal
var FPS = 50;
setInterval(function(){
    principal();
},1000/FPS);

function principal(){
   borraCanvas();
   Gravedad();
  
   dibujarsuelo();
   dibujarnube1();
   dibujarnube2();
   dibujarnube3();
   dibujarnube4();
   dibujarnube5();
   dibujarnube6();
   logicaManzana();
   dibujamanzana();
   dibujaninio();
   puntuacacion();
   dibujarfondo();
}