from django.db import models

# Create your models here.
class Persona(models.Model):
    username = models.CharField(max_length=12, primary_key=True)
    name = models.CharField(max_length=25)
    email = models.EmailField()
    password = models.CharField(max_length=20)
    password2 = models.CharField(max_length=20)

