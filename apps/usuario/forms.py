from django import forms
from django.contrib.auth.forms import UserCreationForm
from apps.usuario.models import Persona

class RegistrationForm(UserCreationForm):
    
    class Meta:
        model = Persona
        fields = ("username", "name", "email", "password", "password2")
        
  
